/*
Diciplina:
    Processamento de Imagens


Componentes dos grupos:
    Samuel Deboni Fraga
    Rphael de Paula
    Igor Fagundes
    Helton Reis
*/

// Trains the neural network with 250 randon images
static void
train_once() {
    for (int i = 0; i < 250; i++) {
        int t = rand() % 6000;

        double t_data[28*28];
        for (int j = 0; j < 28*28; j++) {
            t_data[j] = (double)m_proj.images[t].data[j] / 255.0;
        }

        double o_data[10];
        for (int j = 0; j < 10; j++) {
            if (j == m_proj.labels[t]) {
                o_data[j] = 1.0;
            } else {
                o_data[j] = 0.0;
            }
        }

        genann_train(minst_ann, t_data, o_data, 0.1);
    }
}

// Run the neural network to predict the value of m_image, the result is a number from 0 to 10
static int
run_minst_ann(MinstImage *m_image) {

    double i_data[28*28];
    for (int j = 0; j < 28*28; j++) {
        i_data[j] = (double)m_image->data[j] / 255.0;
    }

    const double *out = genann_run(minst_ann, i_data);

    int res = 0;
    double best = 0;
    for (int i = 0; i < 10; i++) {
        if (out[i] > best) {
            best = out[i];
            res = i;
        }
    }

    return res;
}

// Predict the value using mahalanobis distance, it does not work
static int
value_with_mahalanobis(MinstImage *m_image) {

    cv::Mat s_mat = cv::Mat((int)28, 28, 6);

    for (int idx = 0; idx < 6000; idx++) {
        ((float*)s_mat.data)[idx] = (double)m_image->data[idx] / 255.0f;
    }

    int res = 0;
    double dist = 99999;
    for (int idx = 0; idx < 20; idx++) {
        printf("types = %d %d %d\n", s_mat.type(), covar_samples[idx].type(), icovar.type());
        double c_dist = cv::Mahalanobis(s_mat, covar_samples[idx], icovar);
        if (c_dist < dist) {
            dist = c_dist;
            res = m_proj.labels[idx];
        }
    }


    return res;
}