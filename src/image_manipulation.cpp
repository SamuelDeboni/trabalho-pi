/*
Diciplina:
    Processamento de Imagens


Componentes dos grupos:
    Samuel Deboni Fraga
    Rphael de Paula
    Igor Fagundes
    Helton Reis
*/

#include "image_manipulation.h"

// Read a entire file, allocates the memory usin a SdfArena
static void *
read_file(const char *path, SdfArena *arena, size_t *out_size)
{
    size_t size; 

    FILE *fn = fopen(path, "rb");
    fseek(fn, 0, SEEK_END);
    size = ftell(fn);
    fseek(fn, 0, SEEK_SET);
    
    void *b = sdf_arena_alloc(arena, size);
    fread(b, 1, size, fn);
    
    fclose(fn);

    if (out_size) *out_size = size;
    
    return b;
}

// Decode the Minst Dataset
static MinstImages
minst_from_data(void *im, void *label)
{
    MinstImages result = {0};

    u8 *l = (u8 *)label;
    l += 4;
    result.l_count += l[3];
    result.l_count += (u32)(l[2]) << 8;
    result.l_count += (u32)(l[1]) << 16;
    result.l_count += (u32)(l[1]) << 24;
    result.labels  = (u8 *)((u8 *)label + 8);


    u8 *i = (u8 *)im;
    i += 4;
    result.i_count += i[3];
    result.i_count += (u32)(i[2]) << 8;
    result.i_count += (u32)(i[1]) << 16;
    result.i_count += (u32)(i[1]) << 24;
    result.images  = (MinstImage *)((u8 *)im + 16);

    return result;
}

// Project a image
static void
project_image(MinstImage *im, MinstImage *rm)
{
    // X projection
    for (int x = 0; x < 28; x++) {
        int y = 27;

        for (int i = 0; i < 28; i++) {
            if (im->data_yx[i][x] > 128) {
                rm->data_yx[y--][x] = 255; 
            }
        }
    }

    // Y Projection
    for (int y = 0; y < 28; y++) {
        int x = 0;

        for (int i = 0; i < 28; i++) {
            if (im->data_yx[y][i] > 128) {
                rm->data_yx[y][x++] = 255; 
            }
        }
    }
}

// Project a array of MinstImage
static MinstImages
project_minst_image(MinstImages *ims, SdfArena *arena)
{
    MinstImages result = {0};

    result.labels = (u8 *)sdf_arena_alloc(arena, ims->l_count);
    result.images = (MinstImage *)sdf_arena_alloc(arena, ims->i_count * sizeof(MinstImage));

    for (int i = 0; i < ims->l_count; i++) {
        result.labels[i] = ims->labels[i];
    }

    for (int idx = 0; idx < ims->l_count; idx++) {
        MinstImage im = ims->images[idx];
        MinstImage *rm = &result.images[idx];

        // X projection
        for (int x = 0; x < 28; x++) {
            int y = 27;

            for (int i = 0; i < 28; i++) {
                if (im.data_yx[i][x] > 128) {
                    rm->data_yx[y--][x] = 255; 
                }
            }
        }

        // Y Projection
        for (int y = 0; y < 28; y++) {
            int x = 0;

            for (int i = 0; i < 28; i++) {
                if (im.data_yx[y][i] > 128) {
                    rm->data_yx[y][x++] = 255; 
                }
            }
        }
    }

    return result;
}
