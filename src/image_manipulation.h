/*
Diciplina:
    Processamento de Imagens


Componentes dos grupos:
    Samuel Deboni Fraga
    Rphael de Paula
    Igor Fagundes
    Helton Reis
*/

#ifndef IMAGE_MANIPULATION_H
#define IMAGE_MANIPULATION_H

#define IM_SIZE (28*28)

// A array of bytes that represent the Minst Dataset
union MinstImage {
    u8 data[IM_SIZE];
    u8 data_yx[28][28];
};

// A array of Minst Images and its labels
struct  MinstImages{
    u32 l_count;
    u8 *labels;

    u32 i_count;
    MinstImage *images;
};

static void *read_file(const char *path, SdfArena *arena, size_t *out_size);

#endif // IMAGE_MANIPULATION_H