/*
Diciplina:
	Processamento de Imagens


Componentes dos grupos:
	Samuel Deboni Fraga
	Rphael de Paula
	Igor Fagundes
	Helton Reis
*/


// Include libs
#define SDF_MEM_IMPLEMENTATION
#include "sdf_lib/sdf_mem.h"

#define SDF_ALLOCATORS
#include "sdf_lib/sdf_plataform/sdf_plataform_linux.c"

// Set some types
typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t  i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float  f32;
typedef double f64;

// Useful macros
#define KILO(x) ((i64)(x) * (i64)1024)
#define MEGA(x) (KILO(x) * (i64)1024)
#define GIGA(x) (MEGA(x) * (i64)1024)

// Include lib headers
#include "raylib.h"
#include "genann.h"
#include "image_manipulation.h"

#define RAYGUI_IMPLEMENTATION
#include "raygui.h"

#include <stdio.h>
#include <stdint.h>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

// This represente the app state
// Training or Using
enum AppState {
    APP_STATE_TRAINING = 0,
    APP_STATE_USING    = 1,
};

// Globals
static SdfArena g_arena;
static MinstImages m_training;
static MinstImages m_proj;
static MinstImages m_test;
static MinstImages m_ptest;
static u32 image_index;
static genann *minst_ann;
static AppState app_state;
static double training_rate = 0.1;

static float current_image_alpha = 1;
static int current_image_lower = 128;
static cv::Mat current_image;
static cv::Mat processed_image;
static Texture current_image_tex;
static int selecting = 0;

static int queue_values[64];
static Texture queue_textures[64];
static Texture queue_proj_textures[64];
static MinstImage image_queue[64];
static MinstImage image_proj_queue[64];
static int image_queue_count = 0;
static Vector2 select_start;
static Vector2 select_end;

static cv::Mat covar_samples[6000];
static cv::Mat covar  = cv::Mat(28, 28, 6);
static cv::Mat cmean  = cv::Mat(28, 28, 6);
static cv::Mat icovar = cv::Mat(28, 28, 6);

// Window width and height
int w_width = 1280;
int w_height = 720;

// Include some files
#include "image_manipulation.cpp"
#include "training.cpp"

// Main Funciont
int
main(int arg_count, char **arg_vector)
{
    // Crate a arena with 1Gb of memory reserved
    g_arena = sdf_arena_create(sdf_alloc(GIGA(1)), GIGA(1));

    { // Load minst dataset for training
        void *label_data = read_file("data/train-labels.idx1-ubyte", &g_arena, 0);
        void *image_data = read_file("data/train-images.idx3-ubyte", &g_arena, 0);

        m_training = minst_from_data(image_data, label_data);
        m_proj = project_minst_image(&m_training, &g_arena);
    }

    { // Load minst dataset for testing
        void *label_data = read_file("data/t10k-labels.idx1-ubyte", &g_arena, 0);
        void *image_data = read_file("data/t10k-images.idx3-ubyte", &g_arena, 0);

        m_test = minst_from_data(image_data, label_data);
        m_ptest = project_minst_image(&m_test, &g_arena);
    }

    // Covariance matrix, do not work
#if 0
    {
        for (int i = 0; i < 6000; i++) {
            covar_samples[i] = cv::Mat((int)28, 28, 5);
            for (int j = 0; j < 28*28; j++) {
                ((float *)covar_samples[i].data)[j] = (double)m_proj.images[i].data[j] / 255.0f;
            }
        }

        cv::calcCovarMatrix(covar_samples, 6000, covar, cmean, cv::COVAR_NORMAL);
        printf("covar type = %d\n", covar.type());
        cv::invert(covar, icovar, cv::DECOMP_SVD);

    }
#endif
    
    // Open window using raylib 
    SetConfigFlags(FLAG_WINDOW_RESIZABLE);
    InitWindow(w_width, w_height, "Trabalho");
    SetWindowMinSize(720, 480);
    SetTargetFPS(60);

    // Load Image
    current_image_tex = LoadTexture("images/drag_here.png");
    
    // Start GUI 
    GuiLoadStyleDefault();

    // test images for display
    Texture2D tex =
        LoadTextureFromImage({m_training.images[1].data, 28, 28, 1, 1});
    Texture2D tex2 =
        LoadTextureFromImage({m_training.images[1].data, 28, 28, 1, 1});

    // Init neural network
    minst_ann = genann_init(28*28, 2, 16, 10);

    // Neural network related variables
    int last_predict = 0;
    int correct_predict = 0;
    bool is_training = false;

    // Main loop
    while(!WindowShouldClose()) {

        // Handle resize
        if (IsWindowResized()) {
            w_width  = GetScreenWidth();
            w_height = GetScreenWidth();
        }

        // Draw background
        BeginDrawing();
        ClearBackground((Color){25, 25, 25, 255});
        DrawRectangle(0, 0, w_width, 50, BLUE);
        DrawRectangleLinesEx((Rectangle){0, 0, (float)w_width, 50}, 3, DARKBLUE);

        // Change to Train mode
        if (GuiButton((Rectangle){10, 10, 130, 30}, "Train")) {
            app_state = APP_STATE_TRAINING;
        }

        // Change to Use mode
        if (GuiButton((Rectangle){140, 10, 130, 30}, "Use")) {
            app_state = APP_STATE_USING;
        }

        if (app_state == APP_STATE_TRAINING) {
            float x = 32;
            float y = 64;

            if (GuiButton((Rectangle){x, y, 150, 30 }, !is_training ? "Start Training" : "Stop Training")) {
                is_training = !is_training;
            }

            y += 32;

            if (is_training) {
                train_once(); 

                // Test neural network and show results
                int v = rand() % 10000;

                UpdateTexture(tex, m_test.images[v].data);
                UpdateTexture(tex2, m_ptest.images[v].data);

                correct_predict = m_ptest.labels[v];
                last_predict = run_minst_ann(&m_ptest.images[v]);
            }

            // Test neural network with a randon Minst image
            if (GuiButton((Rectangle){x, y, 150, 30 }, "Test random")) {
                int v = rand() % 10000;

                UpdateTexture(tex, m_test.images[v].data);
                UpdateTexture(tex2, m_ptest.images[v].data);

                correct_predict = m_ptest.labels[v];
                last_predict = run_minst_ann(&m_ptest.images[v]);
            }

            y += 32;

            // Reset Neural Network
            if (GuiButton((Rectangle){x, y, 150, 30}, "Reset ANN")) {
                genann_free(minst_ann);
                minst_ann = genann_init(28*28, 2, 16, 10);
            } 

            y += 32;

            char buff[255];
            sprintf(buff, "%lf", training_rate);
            training_rate = GuiSlider((Rectangle){x, y, 150, 20}, "T rate", buff, training_rate, 0, 1);

            y += 32;
            sprintf(buff, "last_prediction: %d == %d", last_predict, correct_predict);
            DrawText(buff, x, y, 20, correct_predict == last_predict ? GREEN : RED);

            y += 32;
            DrawTextureEx(tex, (Vector2){x, y}, 0, 4, WHITE);

            y += 128;
            DrawTextureEx(tex2, (Vector2){x, y}, 0, 4, WHITE);
        }

        if (app_state == APP_STATE_USING) {
            int count = 0;
            char **dropped_names = GetDroppedFiles(&count);

            // Load draged image to the buffer
            if (count > 0) {
                current_image = cv::imread(dropped_names[0], cv::IMREAD_GRAYSCALE);
                int width = 512 * ((f32)current_image.cols / (f32)current_image.rows);
                cv::resize(current_image, current_image, cv::Size(width, 512), cv::INTER_LINEAR);

                // Invert image
                cv::bitwise_not(current_image, current_image);

                current_image_tex = LoadTextureFromImage({(u8 *)current_image.data, current_image.cols, current_image.rows, 1, 1});

                
            }
            ClearDroppedFiles();

            float x = 32;
            float y = 64;
            DrawTexture(current_image_tex, 232, 64, WHITE);

            float prev_alpha = current_image_alpha;

            char buff[255];

            // HACK: this just copy the current image to the processed image, change later
            current_image.convertTo(processed_image, -1, current_image_alpha, 0);

            // Threshold slider
            sprintf(buff, "%d", current_image_lower);
            current_image_lower = GuiSlider((Rectangle){x, y, 150, 20}, "Lower", buff, current_image_lower, 0, 255);
            y += 32;

            // Filter the image using a threshold
            for (int i = 0;
                     i < current_image.cols * current_image.rows;
                     i++)
            {
                if (((u8 *)current_image.data)[i] < current_image_lower) {
                    ((u8 *)processed_image.data)[i] = 0;
                } else {
                    ((u8 *)processed_image.data)[i] = 255;    
                }
            }

            UpdateTexture(current_image_tex, (u8 *)processed_image.data);

            // Select a single number manualy
            if (selecting > 0) {

                if (IsMouseButtonPressed(0)) {
                    select_start = GetMousePosition();
                    selecting = 2;
                }
                select_end = GetMousePosition();                  

                if (selecting > 1) {
                    Vector2 size = select_end;
                    size.x -= select_start.x;
                    size.y -= select_start.y;
                    size.x = size.x > 0 ? size.x : 0.0;
                    size.y = size.y > 0 ? size.y : 0.0;

                    DrawRectangleLines(select_start.x, select_start.y, size.x, size.y, RED);
                }

                if (IsMouseButtonReleased(0)) {
                    select_end = GetMousePosition();                    
                    select_start.x -= 232;
                    select_start.y -= 64;

                    select_end.x -= 232;
                    select_end.y -= 64;

                    select_start.x = select_start.x > 0 ? select_start.x : 0.0;
                    select_start.y = select_start.y > 0 ? select_start.y : 0.0;

                    select_end.x = select_end.x > 0 ? select_end.x : 0.0;
                    select_end.y = select_end.y > 0 ? select_end.y : 0.0;

                    select_end.x = select_end.x < processed_image.cols ? select_end.x : processed_image.cols - 1;
                    select_end.y = select_end.y < processed_image.rows ? select_end.y : processed_image.rows - 1;

                    int idx = image_queue_count++;

                    cv::Mat cropped_image = processed_image(
                        cv::Range(select_start.y, select_end.y),
                        cv::Range(select_start.x, select_end.x)
                    );

                    int width = 28 * ((f32)cropped_image.cols / (f32)cropped_image.rows);

                    if (width > 28) width = 28;
                    cv::resize(cropped_image, cropped_image, cv::Size(width, 28), cv::INTER_LINEAR);

                    for (int i = 0; i < 28*28; i++) {
                        image_queue[idx].data[i] = 0;
                    }

                    for (int y = 0; y < 28; y++) {
                        for (int x = 0; x < width; x++) {
                            int start = (28 - width) / 2;
                            image_queue[idx].data[(x + start) + y * 28] = ((u8 *)cropped_image.data)[x + y * width];
                        }
                    }

                    project_image(&image_queue[idx], &image_proj_queue[idx]);

                    queue_textures[idx] = LoadTextureFromImage({image_queue[idx].data, 28, 28, 1, 1});
                    queue_proj_textures[idx] = LoadTextureFromImage({image_proj_queue[idx].data, 28, 28, 1, 1});

                    selecting = 0;
                }
            }

            // Draw selected images
            for (int i = 0; i < image_queue_count; i++) {
                DrawTexture(queue_textures[i], 232 + i * 32, 512 + 68, WHITE);
                DrawTexture(queue_proj_textures[i], 232 + i * 32, 512 + 100, WHITE);
            }

            // Start single select
            if (GuiButton((Rectangle){x, y, 150, 30}, "Select")) {
                selecting = 1;
            } 
            y += 32;

            // Auto select the digits
            if (GuiButton((Rectangle){x, y, 150, 30}, "Auto Select")) {
                bool is_white_x[processed_image.cols];

                for (int x = 0; x < processed_image.cols; x += 1) {
                    is_white_x[x] = false;

                    for (int y = 0; y < processed_image.rows; y += 1) {
                        if (processed_image.data[processed_image.cols*y + x] != 0) {
                            is_white_x[x] = true;
                            break;
                        }
                    }
                }

                int x = 0;
                while (x < processed_image.cols) {
                    for (; x < processed_image.cols && !is_white_x[x]; x += 1) {}
                    int init_x = x;
                    for (; x < processed_image.cols && is_white_x[x]; x += 1) {}
                    int end_x  = x;

                    int init_y = 1000000;
                    int end_y  = 0;

                    for (int x2 = init_x; x2 < end_x; x2 += 1) {
                        for (int y = 0; y < processed_image.rows; y += 1) {
                            int value = processed_image.data[processed_image.cols*y + x2];
                            if (value != 0) {
                                if (y < init_y) init_y = y;
                                if (y > end_y)  end_y  = y;
                            }
                        }
                    }

                    if ((end_x - init_x) == 0 || (end_y - init_y) == 0) continue;

                    int idx = image_queue_count++;

                    int vertical_border = (int)((float)(end_y - init_y)*0.3);
                    //int vertical_border = 0;

                    init_y -= vertical_border;
                    end_y  += vertical_border;

                    if (init_y < 0) init_y = 0;
                    if (end_y >= processed_image.rows) end_y = processed_image.rows - 1; 

                    printf("vb = %d, y_init = %d, y_end = %d\n", vertical_border, init_y, end_y);

                    if ((end_y - init_y) == 28) end_y += 1;

                    cv::Mat cropped_image = processed_image(
                        cv::Range(init_y, end_y),
                        cv::Range(init_x, end_x)
                    );

                    int width = 28 * ((f32)cropped_image.cols / (f32)cropped_image.rows);

                    if (width > 28) width = 28;
                    cv::resize(cropped_image, cropped_image, cv::Size(width, 28), cv::INTER_LINEAR);

                    for (int i = 0; i < 28*28; i++) {
                        image_queue[idx].data[i] = 0;
                    }

                    for (int y = 0; y < 28; y++) {
                        for (int x = 0; x < width; x++) {
                            int start = (28 - width) / 2;
                            image_queue[idx].data[(x + start) + y * 28] = ((u8 *)cropped_image.data)[x + y * width];
                        }
                    }

                    project_image(&image_queue[idx], &image_proj_queue[idx]);

                    queue_textures[idx] = LoadTextureFromImage({image_queue[idx].data, 28, 28, 1, 1});
                    queue_proj_textures[idx] = LoadTextureFromImage({image_proj_queue[idx].data, 28, 28, 1, 1});
                
                }
            } 
            y += 32;

            // Reset selction queue
            if (GuiButton((Rectangle){x, y, 150, 30}, "Reset Queue")) {
                image_queue_count = 0;
            } 
            y += 32;

            // Run the selection using the neural network
            if (GuiButton((Rectangle){x, y, 150, 30}, "Run ANN")) {
                for (int i = 0; i < image_queue_count; i++) {
                    queue_values[i] = run_minst_ann(&image_proj_queue[i]);
                }
            } 

            y += 32;

            // Run the selection using the Mahalanobis, does not work
            if (GuiButton((Rectangle){x, y, 150, 30}, "Run Mahalanobis")) {
                for (int i = 0; i < image_queue_count; i++) {
                    //queue_values[i] = value_with_mahalanobis(&image_proj_queue[i]);
                }
            } 

            // Draw results for the selection queue
            for (int i = 0; i < image_queue_count; i++) {
                sprintf(buff, "%d", queue_values[i]);
                DrawText(buff, 232.0f + i * 32.0f, 512.0f + 132.0f, 32, WHITE);
            }
        }

        DrawFPS(w_width - 100, 10);
        EndDrawing();
    }
    
    CloseWindow();
  
    return 0;
}
