O programa é uma rede neural com aprendizado genetico
cujo o objetivo é reconhecer 1 numero de 0 a 9 em uma
imagem 32x32. A rede não é pre treinada, então o teste
será treinar por 5 gerações com uma base de dados na
pasta AI.

Para compilar é somente rodar o script `build.sh`:

./build.sh

para rodar o programa:

./trab <num_threads> 

o argumento deve ser o numero de threads
ex:

./trab 4

rodar com 4 threads
