#!/bin/sh
CC=clang++
IN=src/main.cpp
OUT=trab

LIBS='-lGL -lm -lpthread -ldl -lrt -lX11 -lopencv_core -lopencv_imgcodecs -lopencv_imgproc'

clang -c src/genann.c -o gennan.o -Iinclude
llvm-ar rc lib/gennan.a gennan.o 
rm gennan.o

$CC $IN -O2 lib/libraylib.a lib/gennan.a -o $OUT -Iinclude $LIBS 

[[ $1 = "run" ]] && gf2 trab
